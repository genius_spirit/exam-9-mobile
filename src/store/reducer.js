import {FETCH_POST_SUCCESS, GET_ID, NULL_ID} from "./actions";

const initialState = {
  contacts: [],
  id: ''
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case GET_ID:
      return {...state, id: action.id};
    case FETCH_POST_SUCCESS:
      return {...state, contacts: action.data};
    default:
      return state;
  }
};

export default reducer;