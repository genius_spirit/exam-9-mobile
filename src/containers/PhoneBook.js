import React, { Component } from 'react';
import {connect} from "react-redux";
import {View, FlatList, StyleSheet, Text, Button, Modal, TouchableOpacity, Image} from "react-native";
import ContactItem from "../components/ContactItem";
import {getContacts, getId} from "../store/actions";

class PhoneBook extends Component {

  state = {
    modalVisible: false
  };

  componentDidMount() {
    this.props.onGetContacts();
  }

  toggleModal = () => {
    this.setState(prevState => {
      return { modalVisible: !prevState.modalVisible}
    });
  };

  openHandle = (id) => {
    this.toggleModal();
    this.props.onGetId(id)
  };

  render() {
    return(
      <View style={styles.container}>
        <FlatList
          keyExtractor={(item, index) => index}
          style={styles.contactsList}
          data={Object.keys(this.props.contacts)}
          renderItem={({item}) => (
            <ContactItem
              image={this.props.contacts[item].photo}
              name={this.props.contacts[item].name}
              clicked={() => this.openHandle(item)}
            />
          )}
        />
        <Modal
          animationType="slide"
          visible={this.state.modalVisible}
          onRequestClose={this.toggleModal}>
          {
            this.props.id ?
              <View style={styles.contactView}>
                <View style={styles.contact}>
                  <Text style={styles.text}>{this.props.contacts[this.props.id].name}</Text>
                  <Image resizeMode='contain' source={{uri: this.props.contacts[this.props.id].photo}} style={styles.imageModal}/>
                  <Text style={styles.text}>{this.props.contacts[this.props.id].phone}</Text>
                  <Text style={styles.text}>{this.props.contacts[this.props.id].email}</Text>
                </View>
                <Button title="Back to list" onPress={this.toggleModal}/>
              </View> : null
          }
        </Modal>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
  contactsList: {
    width: '100%',
    padding: 10
  },
  contactView: {
    flex: 1,
    justifyContent: 'flex-start',
    alignItems: 'center'
  },
  contact: {
    flex: 1
  },
  image: {
    width: 150,
    height: 100,
    marginRight: 10
  },
  imageModal: {
    width: 150,
    height: 100,
    marginRight: 10,
    marginBottom: 20
  },
  text: {
    fontSize: 30,
    marginBottom: 20
  }
});

const mapStateToProps = state => {
  return {
    contacts: state.contacts,
    id: state.id
  }
};

const mapDispatchToProps = dispatch => {
  return {
    onGetContacts: () => dispatch(getContacts()),
    onGetId: (id) => dispatch(getId(id))
  }
};

export default connect(mapStateToProps, mapDispatchToProps)(PhoneBook);